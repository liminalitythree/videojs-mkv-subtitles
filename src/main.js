const ebml = require('ebml')
var hyperquest = require('hyperquest')



var subArray = [] // the array for the subtitles to be in


function handleStream (stream) {
  const decoder = new ebml.Decoder()
  const tracks = []
  const trackData = []
  const files = []
  let currentFile = 0
  let currentTimecode
  let trackIndexTemp
  let trackTypeTemp
  let trackDataTemp
  let trackIndex
  var currentDuration = 1500


  decoder.on('error', (error) => {
    console.log('decoder error')
    stream.destroy()
  })

  decoder.on('data', (chunk) => {
    switch (chunk[0]) {
      case 'end':
        // if (chunk[1].name === 'Info') {
        //   stream.destroy()
        // }
        if (chunk[1].name === 'TrackEntry') {
          if (trackTypeTemp === 0x11) {
            tracks.push(trackIndexTemp)
            trackData.push([trackDataTemp])
          }
        }
        break
      case 'tag':
        if (chunk[1].name === 'FileName') {
          if (!files[currentFile]) files[currentFile] = {}
          files[currentFile].name = chunk[1].data.toString()
        }
        if (chunk[1].name === 'FileData') {
          if (!files[currentFile]) files[currentFile] = {}
          files[currentFile].data = chunk[1].data
        }
        if (chunk[1].name === 'TrackNumber') {
          trackIndexTemp = chunk[1].data[0]
        }
        if (chunk[1].name === 'TrackType') {
          trackTypeTemp = chunk[1].data[0]
        }
        if (chunk[1].name === 'CodecPrivate') {
          trackDataTemp = chunk[1].data.toString()
        }
        if (chunk[1].name === 'SimpleBlock' || chunk[1].name === 'Block') {
          const trackLength = ebml.tools.readVint(chunk[1].data)
          trackIndex = tracks.indexOf(trackLength.value)
          if (trackIndex !== -1) {
            const timestampArray = new Uint8Array(chunk[1].data)
              .slice(trackLength.length, trackLength.length + 2)
            const timestamp = new DataView(timestampArray.buffer).getInt16(0)
            const lineData = chunk[1].data.slice(trackLength.length + 3)
            trackData[trackIndex].push(lineData.toString(), timestamp, currentTimecode)

            subDataToArray(lineData.toString(), timestamp, currentTimecode, currentDuration)
          }
        }
        if (chunk[1].name === 'Timecode') {
          const timecode = readUnsignedInteger(padZeroes(chunk[1].data))
          currentTimecode = timecode
        }
        if (chunk[1].name === 'BlockDuration') {
          // the duration is in milliseconds
          const duration = readUnsignedInteger(padZeroes(chunk[1].data))
          trackData[trackIndex].push(duration)
          currentDuration = duration
        }
        break
    }
    if (files[currentFile] && files[currentFile].name && files[currentFile].data) {
      currentFile++
    }
  })

  stream.on('end', () => {
    console.log('subs done mate')
  })

  stream.pipe(decoder)
}

function padZeroes (arr) {
  const len = Math.ceil(arr.length / 2) * 2
  const output = new Uint8Array(len)
  output.set(arr, len - arr.length)
  return output.buffer
}

function readUnsignedInteger (data) {
  const view = new DataView(data)
  return data.byteLength === 2
    ? view.getUint16(0)
    : view.getUint32(0)
}

function formatTimestamp (timestamp) {
  const seconds = timestamp / 1000
  const hh = Math.floor(seconds / 3600)
  let mm = Math.floor((seconds - (hh * 3600)) / 60)
  let ss = (seconds - (hh * 3600) - (mm * 60)).toFixed(2)

  if (mm < 10) mm = `0${mm}`
  if (ss < 10) ss = `0${ss}`

  return `${hh}:${mm}:${ss}`
}

function formatTimestampSRT (timestamp) {
  const seconds = timestamp / 1000
  let hh = Math.floor(seconds / 3600)
  let mm = Math.floor((seconds - (hh * 3600)) / 60)
  let ss = (seconds - (hh * 3600) - (mm * 60)).toFixed(3)

  if (hh < 10) hh = `0${hh}`
  if (mm < 10) mm = `0${mm}`
  if (ss < 10) ss = `0${ss}`

  return `${hh}:${mm}:${ss}`
}

function formatDuration (duration) {
  // Moment.js is the new jQuery
  duration = Math.round(duration)
  if (duration < 2) return 'few seconds'
  if (duration < 58) return duration + ' seconds'
  if (duration < 120) return '1 minute'
  if (duration < 3598) return Math.floor(duration / 60) + ' minutes'
  if (duration < 7200) return '2 hours'
  return Math.floor(duration / 3600) + ' hours'
}

function stringFromThingie(thingie) {
  return thingie.slice(thingie.split(',',8).join(',').length + 1)  
}

function subDataToArray(thingie, timestamp, timecode, duration) {
  var datatime = timecode + timestamp
  var datatext = stringFromThingie(thingie)
  subArray.push({
    text: datatext,
    time: datatime,
    dur: duration
  })
  var startsec = datatime / 1000.0
  var endsec = startsec + (duration / 1000.0)

  if (datatext.includes("\\N")) {
    track.addCue(new VTTCue(startsec, endsec, datatext.split("\\N")[1]));
    track.addCue(new VTTCue(startsec, endsec, datatext.split("\\N")[0]));
  } else {
    track.addCue(new VTTCue(startsec, endsec, datatext));
  }
}

var video = document.getElementById("video"), track;
var sources = video.getElementsByTagName('source');

video.addEventListener("loadedmetadata", function() {
   track = this.addTextTrack("captions", "English", "en")
   track.mode = "showing"
   track.addCue(new VTTCue(0, 1, "[Test]"))
   handleStream(hyperquest(sources[0].src))
   console.log(sources[0].src)
});




